
import utfpr.ct.dainf.if62c.pratica.Circulo;
import utfpr.ct.dainf.if62c.pratica.Elipse;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author a1906399
 */
public class Pratica42 {
    public static void main(String[] args) {
        Elipse elipse = new Elipse(2,3);
	Circulo circulo = new Circulo(4);

	double area, perimetro;

	area = elipse.getArea();
	perimetro = elipse.getPerimetro();

	System.out.println("Area da elipse: " + area);
	System.out.println("Perimetro da elipse: " + perimetro);

	area = circulo.getArea();
	perimetro = circulo.getPerimetro();

	System.out.println("Area do circulo: " + area);
	System.out.println("Perimetro do circulo: " + perimetro);

    }
}
